<?php
/**
 * Created by PhpStorm.
 * User: Noa
 * Date: 07/06/2018
 * Time: 23:08
 */

use Illuminate\Support\Facades\Route;

Route::get('/oauth2', 'noa\oauth2\Oauth2Controller@index');