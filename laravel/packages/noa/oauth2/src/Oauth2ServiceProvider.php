<?php

namespace Noa\Oauth2;

use Illuminate\Support\ServiceProvider;

class Oauth2ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes/web.php';
        $this->app->make(Oauth2Controller::class);
    }
}
